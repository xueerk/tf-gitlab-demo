# AWS Configure
variable "aws_region" {
  type = string
  description = "AWS region to use"
  default = "ap-southeast-1"
}

variable "project_name" {
  type = string
  description = "The value of tag project which will be attached to provisioned resources"
}

variable "asg_desired_capacity" {
  type = number
  description = "The desired capacity of auto-scaling group"
  default = 1
}
