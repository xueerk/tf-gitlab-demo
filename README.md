# tf-gitlab-sample

**1.** terraform workflow![terrafrom-diagram](pic/terraform.png)
- day 1 provision: 
  - terraform init -> terraform plan -> terraform apply
  - init: initialize terraform working environment, statefile will be uploaded to remote backend (e.g. gitlab HTTP backend)
  - plan: creates an execution plan that proposes a set of change actions which make the remote objects match the configuration
  - apply: executes the actions proposed in a terraform plan.<br/>
- day 2 operation:
  - terraform plan -> terraform apply<br/>
- future state with compliance check
  - terraform init -> terraform plan -> compliance check -> terraform apply<br/>
- example aws infra:![aws-infra-diagram](pic/aws_infra.jpg)

**2.** terraform integration with gitlab CI/CD pipeline
- build the CI/CD pipeline
  - add the .gitlab-ci.yml file
  - set up the variables in gitlab<br/>
- sample workflow
  - change the user variable in a branch
  - open an MR to review changes
  - merge the change<br/>
